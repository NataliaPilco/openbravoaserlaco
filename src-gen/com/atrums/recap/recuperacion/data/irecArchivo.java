/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.recap.recuperacion.data;

import com.atrums.recap.data.atrecRecapLinea;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity IREC_Archivo (stored in table IREC_archivo).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class irecArchivo extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "IREC_archivo";
    public static final String ENTITY_NAME = "IREC_Archivo";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_ATRECRECAPLINEA = "atrecRecapLinea";
    public static final String PROPERTY_LOTE = "lote";
    public static final String PROPERTY_FECHAPAGO = "fechaPago";
    public static final String PROPERTY_VALORDEPOSITO = "valorDeposito";
    public static final String PROPERTY_VALORPAGADO = "valorPagado";
    public static final String PROPERTY_ERRORMSG = "errormsg";
    public static final String PROPERTY_ISIMPORTED = "isimported";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_VALORCOMISION = "valorComision";
    public static final String PROPERTY_VALORIVA = "valorIva";
    public static final String PROPERTY_VALORRENTA = "valorRenta";
    public static final String PROPERTY_FECHARECAP = "fechaRecap";
    public static final String PROPERTY_BANCO = "banco";

    public irecArchivo() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_ISIMPORTED, false);
        setDefaultValue(PROPERTY_PROCESSED, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public atrecRecapLinea getAtrecRecapLinea() {
        return (atrecRecapLinea) get(PROPERTY_ATRECRECAPLINEA);
    }

    public void setAtrecRecapLinea(atrecRecapLinea atrecRecapLinea) {
        set(PROPERTY_ATRECRECAPLINEA, atrecRecapLinea);
    }

    public String getLote() {
        return (String) get(PROPERTY_LOTE);
    }

    public void setLote(String lote) {
        set(PROPERTY_LOTE, lote);
    }

    public String getFechaPago() {
        return (String) get(PROPERTY_FECHAPAGO);
    }

    public void setFechaPago(String fechaPago) {
        set(PROPERTY_FECHAPAGO, fechaPago);
    }

    public BigDecimal getValorDeposito() {
        return (BigDecimal) get(PROPERTY_VALORDEPOSITO);
    }

    public void setValorDeposito(BigDecimal valorDeposito) {
        set(PROPERTY_VALORDEPOSITO, valorDeposito);
    }

    public BigDecimal getValorPagado() {
        return (BigDecimal) get(PROPERTY_VALORPAGADO);
    }

    public void setValorPagado(BigDecimal valorPagado) {
        set(PROPERTY_VALORPAGADO, valorPagado);
    }

    public String getErrormsg() {
        return (String) get(PROPERTY_ERRORMSG);
    }

    public void setErrormsg(String errormsg) {
        set(PROPERTY_ERRORMSG, errormsg);
    }

    public Boolean isImported() {
        return (Boolean) get(PROPERTY_ISIMPORTED);
    }

    public void setImported(Boolean isimported) {
        set(PROPERTY_ISIMPORTED, isimported);
    }

    public Boolean isProcessed() {
        return (Boolean) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(Boolean processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public BigDecimal getValorComision() {
        return (BigDecimal) get(PROPERTY_VALORCOMISION);
    }

    public void setValorComision(BigDecimal valorComision) {
        set(PROPERTY_VALORCOMISION, valorComision);
    }

    public BigDecimal getValorIva() {
        return (BigDecimal) get(PROPERTY_VALORIVA);
    }

    public void setValorIva(BigDecimal valorIva) {
        set(PROPERTY_VALORIVA, valorIva);
    }

    public BigDecimal getValorRenta() {
        return (BigDecimal) get(PROPERTY_VALORRENTA);
    }

    public void setValorRenta(BigDecimal valorRenta) {
        set(PROPERTY_VALORRENTA, valorRenta);
    }

    public String getFechaRecap() {
        return (String) get(PROPERTY_FECHARECAP);
    }

    public void setFechaRecap(String fechaRecap) {
        set(PROPERTY_FECHARECAP, fechaRecap);
    }

    public String getBanco() {
        return (String) get(PROPERTY_BANCO);
    }

    public void setBanco(String banco) {
        set(PROPERTY_BANCO, banco);
    }

}
