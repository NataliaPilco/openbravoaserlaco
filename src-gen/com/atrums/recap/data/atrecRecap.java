/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.recap.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
/**
 * Entity class for entity ATREC_Recap (stored in table atrec_recap).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class atrecRecap extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "atrec_recap";
    public static final String ENTITY_NAME = "ATREC_Recap";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_FINFINANCIALACCOUNT = "fINFinancialAccount";
    public static final String PROPERTY_FECHADECIERRE = "fechaDeCierre";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_TOTALFAC = "totalFac";
    public static final String PROPERTY_TOTALRET = "totalRet";
    public static final String PROPERTY_TOTALCOBRADO = "totalCobrado";
    public static final String PROPERTY_TOTALGASTO = "totalGasto";
    public static final String PROPERTY_TOTALPAGOS = "totalPagos";
    public static final String PROPERTY_DIFERENCIA = "diferencia";
    public static final String PROPERTY_REACTIVAR = "reactivar";
    public static final String PROPERTY_ATRECRECAPCOBRARLIST = "aTRECRecapCobrarList";
    public static final String PROPERTY_ATRECRECAPGASTOLIST = "aTRECRecapGastoList";
    public static final String PROPERTY_ATRECRECAPLINEALIST = "aTRECRecapLineaList";
    public static final String PROPERTY_ATRECRECAPTARJETALIST = "aTRECRecapTarjetaList";

    public atrecRecap() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESSED, false);
        setDefaultValue(PROPERTY_PROCESAR, false);
        setDefaultValue(PROPERTY_TOTALRET, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALCOBRADO, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALGASTO, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALPAGOS, new BigDecimal(0));
        setDefaultValue(PROPERTY_REACTIVAR, false);
        setDefaultValue(PROPERTY_ATRECRECAPCOBRARLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_ATRECRECAPGASTOLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_ATRECRECAPLINEALIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_ATRECRECAPTARJETALIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public FIN_FinancialAccount getFINFinancialAccount() {
        return (FIN_FinancialAccount) get(PROPERTY_FINFINANCIALACCOUNT);
    }

    public void setFINFinancialAccount(FIN_FinancialAccount fINFinancialAccount) {
        set(PROPERTY_FINFINANCIALACCOUNT, fINFinancialAccount);
    }

    public Date getFechaDeCierre() {
        return (Date) get(PROPERTY_FECHADECIERRE);
    }

    public void setFechaDeCierre(Date fechaDeCierre) {
        set(PROPERTY_FECHADECIERRE, fechaDeCierre);
    }

    public Boolean isProcessed() {
        return (Boolean) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(Boolean processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public BigDecimal getTotalFac() {
        return (BigDecimal) get(PROPERTY_TOTALFAC);
    }

    public void setTotalFac(BigDecimal totalFac) {
        set(PROPERTY_TOTALFAC, totalFac);
    }

    public BigDecimal getTotalRet() {
        return (BigDecimal) get(PROPERTY_TOTALRET);
    }

    public void setTotalRet(BigDecimal totalRet) {
        set(PROPERTY_TOTALRET, totalRet);
    }

    public BigDecimal getTotalCobrado() {
        return (BigDecimal) get(PROPERTY_TOTALCOBRADO);
    }

    public void setTotalCobrado(BigDecimal totalCobrado) {
        set(PROPERTY_TOTALCOBRADO, totalCobrado);
    }

    public BigDecimal getTotalGasto() {
        return (BigDecimal) get(PROPERTY_TOTALGASTO);
    }

    public void setTotalGasto(BigDecimal totalGasto) {
        set(PROPERTY_TOTALGASTO, totalGasto);
    }

    public BigDecimal getTotalPagos() {
        return (BigDecimal) get(PROPERTY_TOTALPAGOS);
    }

    public void setTotalPagos(BigDecimal totalPagos) {
        set(PROPERTY_TOTALPAGOS, totalPagos);
    }

    public BigDecimal getDiferencia() {
        return (BigDecimal) get(PROPERTY_DIFERENCIA);
    }

    public void setDiferencia(BigDecimal diferencia) {
        set(PROPERTY_DIFERENCIA, diferencia);
    }

    public Boolean isReactivar() {
        return (Boolean) get(PROPERTY_REACTIVAR);
    }

    public void setReactivar(Boolean reactivar) {
        set(PROPERTY_REACTIVAR, reactivar);
    }

    @SuppressWarnings("unchecked")
    public List<atrecRecapCobrar> getATRECRecapCobrarList() {
      return (List<atrecRecapCobrar>) get(PROPERTY_ATRECRECAPCOBRARLIST);
    }

    public void setATRECRecapCobrarList(List<atrecRecapCobrar> aTRECRecapCobrarList) {
        set(PROPERTY_ATRECRECAPCOBRARLIST, aTRECRecapCobrarList);
    }

    @SuppressWarnings("unchecked")
    public List<atrecRecapGasto> getATRECRecapGastoList() {
      return (List<atrecRecapGasto>) get(PROPERTY_ATRECRECAPGASTOLIST);
    }

    public void setATRECRecapGastoList(List<atrecRecapGasto> aTRECRecapGastoList) {
        set(PROPERTY_ATRECRECAPGASTOLIST, aTRECRecapGastoList);
    }

    @SuppressWarnings("unchecked")
    public List<atrecRecapLinea> getATRECRecapLineaList() {
      return (List<atrecRecapLinea>) get(PROPERTY_ATRECRECAPLINEALIST);
    }

    public void setATRECRecapLineaList(List<atrecRecapLinea> aTRECRecapLineaList) {
        set(PROPERTY_ATRECRECAPLINEALIST, aTRECRecapLineaList);
    }

    @SuppressWarnings("unchecked")
    public List<atrecRecapTarjeta> getATRECRecapTarjetaList() {
      return (List<atrecRecapTarjeta>) get(PROPERTY_ATRECRECAPTARJETALIST);
    }

    public void setATRECRecapTarjetaList(List<atrecRecapTarjeta> aTRECRecapTarjetaList) {
        set(PROPERTY_ATRECRECAPTARJETALIST, aTRECRecapTarjetaList);
    }

}
