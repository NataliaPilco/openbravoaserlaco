//Sqlc generated V1.O00-1
package com.atrums.remica.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class BaseImponibleData implements FieldProvider {
static Logger log4j = Logger.getLogger(BaseImponibleData.class);
  private String InitRecordNumber="0";
  public String taxbaseamt;
  public String taxamt;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("taxbaseamt"))
      return taxbaseamt;
    else if (fieldName.equalsIgnoreCase("taxamt"))
      return taxamt;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static BaseImponibleData[] select(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return select(connectionProvider, cInvoiceId, 0, 0);
  }

  public static BaseImponibleData[] select(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select totallines as taxbaseamt, grandtotal - totallines as taxamt  " +
      "    from c_invoice" +
      "    where c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        BaseImponibleData objectBaseImponibleData = new BaseImponibleData();
        objectBaseImponibleData.taxbaseamt = UtilSql.getValue(result, "taxbaseamt");
        objectBaseImponibleData.taxamt = UtilSql.getValue(result, "taxamt");
        objectBaseImponibleData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectBaseImponibleData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    BaseImponibleData objectBaseImponibleData[] = new BaseImponibleData[vector.size()];
    vector.copyInto(objectBaseImponibleData);
    return(objectBaseImponibleData);
  }
}
