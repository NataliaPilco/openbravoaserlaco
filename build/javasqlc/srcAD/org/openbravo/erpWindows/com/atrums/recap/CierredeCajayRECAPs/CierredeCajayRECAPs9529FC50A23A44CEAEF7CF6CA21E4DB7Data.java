//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.recap.CierredeCajayRECAPs;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data implements FieldProvider {
static Logger log4j = Logger.getLogger(CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String finFinancialAccountId;
  public String finFinancialAccountIdr;
  public String fechaCierre;
  public String processed;
  public String procesar;
  public String reactivar;
  public String totalFac;
  public String totalRet;
  public String totalPagos;
  public String totalCobrado;
  public String totalGasto;
  public String diferencia;
  public String isactive;
  public String atrecRecapId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_idr") || fieldName.equals("finFinancialAccountIdr"))
      return finFinancialAccountIdr;
    else if (fieldName.equalsIgnoreCase("fecha_cierre") || fieldName.equals("fechaCierre"))
      return fechaCierre;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("reactivar"))
      return reactivar;
    else if (fieldName.equalsIgnoreCase("total_fac") || fieldName.equals("totalFac"))
      return totalFac;
    else if (fieldName.equalsIgnoreCase("total_ret") || fieldName.equals("totalRet"))
      return totalRet;
    else if (fieldName.equalsIgnoreCase("total_pagos") || fieldName.equals("totalPagos"))
      return totalPagos;
    else if (fieldName.equalsIgnoreCase("total_cobrado") || fieldName.equals("totalCobrado"))
      return totalCobrado;
    else if (fieldName.equalsIgnoreCase("total_gasto") || fieldName.equals("totalGasto"))
      return totalGasto;
    else if (fieldName.equalsIgnoreCase("diferencia"))
      return diferencia;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("atrec_recap_id") || fieldName.equals("atrecRecapId"))
      return atrecRecapId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atrec_recap.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atrec_recap.CreatedBy) as CreatedByR, " +
      "        to_char(atrec_recap.Updated, ?) as updated, " +
      "        to_char(atrec_recap.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atrec_recap.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atrec_recap.UpdatedBy) as UpdatedByR," +
      "        atrec_recap.AD_Org_ID, " +
      "(CASE WHEN atrec_recap.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "atrec_recap.FIN_Financial_Account_ID, " +
      "(CASE WHEN atrec_recap.FIN_Financial_Account_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.ISO_Code), ''))),'') ) END) AS FIN_Financial_Account_IDR, " +
      "atrec_recap.Fecha_Cierre, " +
      "COALESCE(atrec_recap.Processed, 'N') AS Processed, " +
      "atrec_recap.Procesar, " +
      "atrec_recap.Reactivar, " +
      "atrec_recap.Total_Fac, " +
      "atrec_recap.Total_Ret, " +
      "atrec_recap.Total_Pagos, " +
      "atrec_recap.Total_Cobrado, " +
      "atrec_recap.Total_Gasto, " +
      "atrec_recap.Diferencia, " +
      "COALESCE(atrec_recap.Isactive, 'N') AS Isactive, " +
      "atrec_recap.Atrec_Recap_ID, " +
      "atrec_recap.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atrec_recap left join (select AD_Org_ID, Name from AD_Org) table1 on (atrec_recap.AD_Org_ID = table1.AD_Org_ID) left join (select FIN_Financial_Account_ID, Name, C_Currency_ID from FIN_Financial_Account) table2 on (atrec_recap.FIN_Financial_Account_ID = table2.FIN_Financial_Account_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table3 on (table2.C_Currency_ID = table3.C_Currency_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND atrec_recap.Atrec_Recap_ID = ? " +
      "        AND atrec_recap.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atrec_recap.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data = new CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data();
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.created = UtilSql.getValue(result, "created");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.updated = UtilSql.getValue(result, "updated");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.finFinancialAccountIdr = UtilSql.getValue(result, "fin_financial_account_idr");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.fechaCierre = UtilSql.getDateValue(result, "fecha_cierre", "dd-MM-yyyy");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.processed = UtilSql.getValue(result, "processed");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.procesar = UtilSql.getValue(result, "procesar");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.reactivar = UtilSql.getValue(result, "reactivar");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.totalFac = UtilSql.getValue(result, "total_fac");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.totalRet = UtilSql.getValue(result, "total_ret");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.totalPagos = UtilSql.getValue(result, "total_pagos");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.totalCobrado = UtilSql.getValue(result, "total_cobrado");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.totalGasto = UtilSql.getValue(result, "total_gasto");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.diferencia = UtilSql.getValue(result, "diferencia");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.isactive = UtilSql.getValue(result, "isactive");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.atrecRecapId = UtilSql.getValue(result, "atrec_recap_id");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.language = UtilSql.getValue(result, "language");
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.adUserClient = "";
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.adOrgClient = "";
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.createdby = "";
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.trBgcolor = "";
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.totalCount = "";
        objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[] = new CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[vector.size()];
    vector.copyInto(objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data);
    return(objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data);
  }

/**
Create a registry
 */
  public static CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[] set(String fechaCierre, String adClientId, String adOrgId, String diferencia, String procesar, String reactivar, String totalRet, String totalPagos, String totalGasto, String processed, String totalCobrado, String atrecRecapId, String updatedby, String updatedbyr, String isactive, String finFinancialAccountId, String totalFac, String createdby, String createdbyr)    throws ServletException {
    CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[] = new CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[1];
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0] = new CierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data();
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].created = "";
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].createdbyr = createdbyr;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].updated = "";
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].updatedTimeStamp = "";
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].updatedby = updatedby;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].updatedbyr = updatedbyr;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].adOrgId = adOrgId;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].adOrgIdr = "";
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].finFinancialAccountId = finFinancialAccountId;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].finFinancialAccountIdr = "";
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].fechaCierre = fechaCierre;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].processed = processed;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].procesar = procesar;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].reactivar = reactivar;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].totalFac = totalFac;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].totalRet = totalRet;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].totalPagos = totalPagos;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].totalCobrado = totalCobrado;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].totalGasto = totalGasto;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].diferencia = diferencia;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].isactive = isactive;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].atrecRecapId = atrecRecapId;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].adClientId = adClientId;
    objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data[0].language = "";
    return objectCierredeCajayRECAPs9529FC50A23A44CEAEF7CF6CA21E4DB7Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDefC577313B490C4A689681CF075973B50F_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefFF9C189FD753460C9ADDB0D83E3E8F4F_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atrec_recap" +
      "        SET AD_Org_ID = (?) , FIN_Financial_Account_ID = (?) , Fecha_Cierre = TO_DATE(?) , Processed = (?) , Procesar = (?) , Reactivar = (?) , Total_Fac = TO_NUMBER(?) , Total_Ret = TO_NUMBER(?) , Total_Pagos = TO_NUMBER(?) , Total_Cobrado = TO_NUMBER(?) , Total_Gasto = TO_NUMBER(?) , Diferencia = TO_NUMBER(?) , Isactive = (?) , Atrec_Recap_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atrec_recap.Atrec_Recap_ID = ? " +
      "        AND atrec_recap.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atrec_recap.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reactivar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalFac);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalPagos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalCobrado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalGasto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, diferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrecRecapId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrecRecapId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atrec_recap " +
      "        (AD_Org_ID, FIN_Financial_Account_ID, Fecha_Cierre, Processed, Procesar, Reactivar, Total_Fac, Total_Ret, Total_Pagos, Total_Cobrado, Total_Gasto, Diferencia, Isactive, Atrec_Recap_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), TO_DATE(?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reactivar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalFac);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalPagos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalCobrado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalGasto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, diferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrecRecapId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atrec_recap" +
      "        WHERE atrec_recap.Atrec_Recap_ID = ? " +
      "        AND atrec_recap.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atrec_recap.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atrec_recap" +
      "         WHERE atrec_recap.Atrec_Recap_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atrec_recap" +
      "         WHERE atrec_recap.Atrec_Recap_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
