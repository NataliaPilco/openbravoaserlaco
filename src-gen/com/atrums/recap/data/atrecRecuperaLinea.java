/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.recap.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
/**
 * Entity class for entity ATREC_RecuperaLinea (stored in table atrec_recupera_linea).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class atrecRecuperaLinea extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "atrec_recupera_linea";
    public static final String ENTITY_NAME = "ATREC_RecuperaLinea";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_ATRECRECUPERA = "atrecRecupera";
    public static final String PROPERTY_ATRECRECAPLINEA = "atrecRecapLinea";
    public static final String PROPERTY_LINENO = "lineNo";
    public static final String PROPERTY_FECHAPAGO = "fechaPago";
    public static final String PROPERTY_LOTE = "lote";
    public static final String PROPERTY_VALOR = "valor";
    public static final String PROPERTY_VALORPAGADO = "valorPagado";
    public static final String PROPERTY_VALORPENDIENTE = "valorPendiente";
    public static final String PROPERTY_FINANCIALACCOUNT = "financialAccount";
    public static final String PROPERTY_COBRO = "cobro";
    public static final String PROPERTY_PAGO = "pago";
    public static final String PROPERTY_EMPLEADO = "empleado";
    public static final String PROPERTY_VALORCOMISION = "valorComision";
    public static final String PROPERTY_VALORIVA = "valorIva";
    public static final String PROPERTY_VALORRENTA = "valorRenta";
    public static final String PROPERTY_FINFINANCIALACCOUNT2 = "fINFinancialAccount2";

    public atrecRecuperaLinea() {
        setDefaultValue(PROPERTY_ACTIVE, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public atrecRecupera getAtrecRecupera() {
        return (atrecRecupera) get(PROPERTY_ATRECRECUPERA);
    }

    public void setAtrecRecupera(atrecRecupera atrecRecupera) {
        set(PROPERTY_ATRECRECUPERA, atrecRecupera);
    }

    public atrecRecapLinea getAtrecRecapLinea() {
        return (atrecRecapLinea) get(PROPERTY_ATRECRECAPLINEA);
    }

    public void setAtrecRecapLinea(atrecRecapLinea atrecRecapLinea) {
        set(PROPERTY_ATRECRECAPLINEA, atrecRecapLinea);
    }

    public Long getLineNo() {
        return (Long) get(PROPERTY_LINENO);
    }

    public void setLineNo(Long lineNo) {
        set(PROPERTY_LINENO, lineNo);
    }

    public Date getFechaPago() {
        return (Date) get(PROPERTY_FECHAPAGO);
    }

    public void setFechaPago(Date fechaPago) {
        set(PROPERTY_FECHAPAGO, fechaPago);
    }

    public String getLote() {
        return (String) get(PROPERTY_LOTE);
    }

    public void setLote(String lote) {
        set(PROPERTY_LOTE, lote);
    }

    public BigDecimal getValor() {
        return (BigDecimal) get(PROPERTY_VALOR);
    }

    public void setValor(BigDecimal valor) {
        set(PROPERTY_VALOR, valor);
    }

    public BigDecimal getValorPagado() {
        return (BigDecimal) get(PROPERTY_VALORPAGADO);
    }

    public void setValorPagado(BigDecimal valorPagado) {
        set(PROPERTY_VALORPAGADO, valorPagado);
    }

    public BigDecimal getValorPendiente() {
        return (BigDecimal) get(PROPERTY_VALORPENDIENTE);
    }

    public void setValorPendiente(BigDecimal valorPendiente) {
        set(PROPERTY_VALORPENDIENTE, valorPendiente);
    }

    public FIN_FinancialAccount getFinancialAccount() {
        return (FIN_FinancialAccount) get(PROPERTY_FINANCIALACCOUNT);
    }

    public void setFinancialAccount(FIN_FinancialAccount financialAccount) {
        set(PROPERTY_FINANCIALACCOUNT, financialAccount);
    }

    public FIN_FinaccTransaction getCobro() {
        return (FIN_FinaccTransaction) get(PROPERTY_COBRO);
    }

    public void setCobro(FIN_FinaccTransaction cobro) {
        set(PROPERTY_COBRO, cobro);
    }

    public FIN_FinaccTransaction getPago() {
        return (FIN_FinaccTransaction) get(PROPERTY_PAGO);
    }

    public void setPago(FIN_FinaccTransaction pago) {
        set(PROPERTY_PAGO, pago);
    }

    public BusinessPartner getEmpleado() {
        return (BusinessPartner) get(PROPERTY_EMPLEADO);
    }

    public void setEmpleado(BusinessPartner empleado) {
        set(PROPERTY_EMPLEADO, empleado);
    }

    public BigDecimal getValorComision() {
        return (BigDecimal) get(PROPERTY_VALORCOMISION);
    }

    public void setValorComision(BigDecimal valorComision) {
        set(PROPERTY_VALORCOMISION, valorComision);
    }

    public BigDecimal getValorIva() {
        return (BigDecimal) get(PROPERTY_VALORIVA);
    }

    public void setValorIva(BigDecimal valorIva) {
        set(PROPERTY_VALORIVA, valorIva);
    }

    public BigDecimal getValorRenta() {
        return (BigDecimal) get(PROPERTY_VALORRENTA);
    }

    public void setValorRenta(BigDecimal valorRenta) {
        set(PROPERTY_VALORRENTA, valorRenta);
    }

    public FIN_FinancialAccount getFINFinancialAccount2() {
        return (FIN_FinancialAccount) get(PROPERTY_FINFINANCIALACCOUNT2);
    }

    public void setFINFinancialAccount2(FIN_FinancialAccount fINFinancialAccount2) {
        set(PROPERTY_FINFINANCIALACCOUNT2, fINFinancialAccount2);
    }

}
