//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.recap.RecuperaciondeRECAPs;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData implements FieldProvider {
static Logger log4j = Logger.getLogger(RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String fecha;
  public String descripcion;
  public String processed;
  public String isactive;
  public String procesar;
  public String adClientId;
  public String atrecRecuperaId;
  public String adOrgId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("atrec_recupera_id") || fieldName.equals("atrecRecuperaId"))
      return atrecRecuperaId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atrec_recupera.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atrec_recupera.CreatedBy) as CreatedByR, " +
      "        to_char(atrec_recupera.Updated, ?) as updated, " +
      "        to_char(atrec_recupera.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atrec_recupera.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atrec_recupera.UpdatedBy) as UpdatedByR," +
      "        atrec_recupera.Fecha, " +
      "atrec_recupera.Descripcion, " +
      "COALESCE(atrec_recupera.Processed, 'N') AS Processed, " +
      "COALESCE(atrec_recupera.Isactive, 'N') AS Isactive, " +
      "atrec_recupera.Procesar, " +
      "atrec_recupera.AD_Client_ID, " +
      "atrec_recupera.Atrec_Recupera_ID, " +
      "atrec_recupera.AD_Org_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atrec_recupera" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND atrec_recupera.Atrec_Recupera_ID = ? " +
      "        AND atrec_recupera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atrec_recupera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData = new RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData();
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.created = UtilSql.getValue(result, "created");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.updated = UtilSql.getValue(result, "updated");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.updatedby = UtilSql.getValue(result, "updatedby");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.descripcion = UtilSql.getValue(result, "descripcion");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.processed = UtilSql.getValue(result, "processed");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.isactive = UtilSql.getValue(result, "isactive");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.procesar = UtilSql.getValue(result, "procesar");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.atrecRecuperaId = UtilSql.getValue(result, "atrec_recupera_id");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.language = UtilSql.getValue(result, "language");
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.adUserClient = "";
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.adOrgClient = "";
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.createdby = "";
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.trBgcolor = "";
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.totalCount = "";
        objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[] = new RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[vector.size()];
    vector.copyInto(objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData);
    return(objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData);
  }

/**
Create a registry
 */
  public static RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[] set(String adOrgId, String processed, String atrecRecuperaId, String isactive, String updatedby, String updatedbyr, String fecha, String descripcion, String adClientId, String procesar, String createdby, String createdbyr)    throws ServletException {
    RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[] = new RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[1];
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0] = new RecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData();
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].created = "";
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].createdbyr = createdbyr;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].updated = "";
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].updatedTimeStamp = "";
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].updatedby = updatedby;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].updatedbyr = updatedbyr;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].fecha = fecha;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].descripcion = descripcion;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].processed = processed;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].isactive = isactive;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].procesar = procesar;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].adClientId = adClientId;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].atrecRecuperaId = atrecRecuperaId;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].adOrgId = adOrgId;
    objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData[0].language = "";
    return objectRecuperaciondeRECAPs225A722BAE4D4265959AAE8D7D5C933DData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef5976B31F00784382920412E452A0F32E_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE8ACA4C9228C4935B234F7F3F98F714A_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atrec_recupera" +
      "        SET Fecha = TO_DATE(?) , Descripcion = (?) , Processed = (?) , Isactive = (?) , Procesar = (?) , AD_Client_ID = (?) , Atrec_Recupera_ID = (?) , AD_Org_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atrec_recupera.Atrec_Recupera_ID = ? " +
      "        AND atrec_recupera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atrec_recupera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrecRecuperaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrecRecuperaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atrec_recupera " +
      "        (Fecha, Descripcion, Processed, Isactive, Procesar, AD_Client_ID, Atrec_Recupera_ID, AD_Org_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrecRecuperaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atrec_recupera" +
      "        WHERE atrec_recupera.Atrec_Recupera_ID = ? " +
      "        AND atrec_recupera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atrec_recupera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atrec_recupera" +
      "         WHERE atrec_recupera.Atrec_Recupera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atrec_recupera" +
      "         WHERE atrec_recupera.Atrec_Recupera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
