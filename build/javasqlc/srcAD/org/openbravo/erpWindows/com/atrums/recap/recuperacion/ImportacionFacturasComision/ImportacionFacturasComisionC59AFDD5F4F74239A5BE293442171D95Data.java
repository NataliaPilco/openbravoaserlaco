//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.recap.recuperacion.ImportacionFacturasComision;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data implements FieldProvider {
static Logger log4j = Logger.getLogger(ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String iErrormsg;
  public String adOrgId;
  public String adOrgIdr;
  public String establecimiento;
  public String emision;
  public String numero;
  public String fecha;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String taxid;
  public String value;
  public String valor;
  public String impuesto;
  public String autorizacion;
  public String vencimiento;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String mPricelistId;
  public String mPricelistIdr;
  public String cPaymenttermId;
  public String cPaymenttermIdr;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String mProductId;
  public String mProductIdr;
  public String sustento;
  public String isactive;
  public String procesado;
  public String procesar;
  public String irecFacturaId;
  public String cInvoiceId;
  public String adClientId;
  public String cGlitemId;
  public String iIsimported;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("i_errormsg") || fieldName.equals("iErrormsg"))
      return iErrormsg;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("establecimiento"))
      return establecimiento;
    else if (fieldName.equalsIgnoreCase("emision"))
      return emision;
    else if (fieldName.equalsIgnoreCase("numero"))
      return numero;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("taxid"))
      return taxid;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("valor"))
      return valor;
    else if (fieldName.equalsIgnoreCase("impuesto"))
      return impuesto;
    else if (fieldName.equalsIgnoreCase("autorizacion"))
      return autorizacion;
    else if (fieldName.equalsIgnoreCase("vencimiento"))
      return vencimiento;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_idr") || fieldName.equals("mPricelistIdr"))
      return mPricelistIdr;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_idr") || fieldName.equals("cPaymenttermIdr"))
      return cPaymenttermIdr;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("m_product_idr") || fieldName.equals("mProductIdr"))
      return mProductIdr;
    else if (fieldName.equalsIgnoreCase("sustento"))
      return sustento;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("procesado"))
      return procesado;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("irec_factura_id") || fieldName.equals("irecFacturaId"))
      return irecFacturaId;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("c_glitem_id") || fieldName.equals("cGlitemId"))
      return cGlitemId;
    else if (fieldName.equalsIgnoreCase("i_isimported") || fieldName.equals("iIsimported"))
      return iIsimported;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(IREC_Factura.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IREC_Factura.CreatedBy) as CreatedByR, " +
      "        to_char(IREC_Factura.Updated, ?) as updated, " +
      "        to_char(IREC_Factura.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        IREC_Factura.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IREC_Factura.UpdatedBy) as UpdatedByR," +
      "        IREC_Factura.I_Errormsg, " +
      "IREC_Factura.AD_Org_ID, " +
      "(CASE WHEN IREC_Factura.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "IREC_Factura.Establecimiento, " +
      "IREC_Factura.Emision, " +
      "IREC_Factura.Numero, " +
      "IREC_Factura.Fecha, " +
      "IREC_Factura.C_Bpartner_ID, " +
      "(CASE WHEN IREC_Factura.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "IREC_Factura.Taxid, " +
      "IREC_Factura.Value, " +
      "IREC_Factura.Valor, " +
      "IREC_Factura.Impuesto, " +
      "IREC_Factura.Autorizacion, " +
      "IREC_Factura.Vencimiento, " +
      "IREC_Factura.C_Doctype_ID, " +
      "(CASE WHEN IREC_Factura.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL3.Name IS NULL THEN TO_CHAR(table3.Name) ELSE TO_CHAR(tableTRL3.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "IREC_Factura.M_Pricelist_ID, " +
      "(CASE WHEN IREC_Factura.M_Pricelist_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS M_Pricelist_IDR, " +
      "IREC_Factura.C_Paymentterm_ID, " +
      "(CASE WHEN IREC_Factura.C_Paymentterm_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL6.Name IS NULL THEN TO_CHAR(table6.Name) ELSE TO_CHAR(tableTRL6.Name) END)), ''))),'') ) END) AS C_Paymentterm_IDR, " +
      "IREC_Factura.FIN_Paymentmethod_ID, " +
      "(CASE WHEN IREC_Factura.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "IREC_Factura.M_Product_ID, " +
      "(CASE WHEN IREC_Factura.M_Product_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL9.Name IS NULL THEN TO_CHAR(table9.Name) ELSE TO_CHAR(tableTRL9.Name) END)), ''))),'') ) END) AS M_Product_IDR, " +
      "IREC_Factura.Sustento, " +
      "COALESCE(IREC_Factura.Isactive, 'N') AS Isactive, " +
      "COALESCE(IREC_Factura.Procesado, 'N') AS Procesado, " +
      "IREC_Factura.Procesar, " +
      "IREC_Factura.Irec_Factura_ID, " +
      "IREC_Factura.C_Invoice_ID, " +
      "IREC_Factura.AD_Client_ID, " +
      "IREC_Factura.C_Glitem_ID, " +
      "COALESCE(IREC_Factura.I_Isimported, 'N') AS I_Isimported, " +
      "        ? AS LANGUAGE " +
      "        FROM IREC_Factura left join (select AD_Org_ID, Name from AD_Org) table1 on (IREC_Factura.AD_Org_ID = table1.AD_Org_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (IREC_Factura.C_Bpartner_ID = table2.C_BPartner_ID) left join (select C_Doctype_ID, Name from C_Doctype) table3 on (IREC_Factura.C_Doctype_ID = table3.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL3 on (table3.C_DocType_ID = tableTRL3.C_DocType_ID and tableTRL3.AD_Language = ?)  left join (select M_Pricelist_ID, Name from M_Pricelist) table5 on (IREC_Factura.M_Pricelist_ID = table5.M_Pricelist_ID) left join (select C_Paymentterm_ID, Name from C_Paymentterm) table6 on (IREC_Factura.C_Paymentterm_ID = table6.C_Paymentterm_ID) left join (select C_PaymentTerm_ID,AD_Language, Name from C_PaymentTerm_TRL) tableTRL6 on (table6.C_PaymentTerm_ID = tableTRL6.C_PaymentTerm_ID and tableTRL6.AD_Language = ?)  left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table8 on (IREC_Factura.FIN_Paymentmethod_ID = table8.FIN_Paymentmethod_ID) left join (select M_Product_ID, Name from M_Product) table9 on (IREC_Factura.M_Product_ID = table9.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL9 on (table9.M_Product_ID = tableTRL9.M_Product_ID and tableTRL9.AD_Language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND IREC_Factura.Irec_Factura_ID = ? " +
      "        AND IREC_Factura.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND IREC_Factura.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data = new ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data();
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.created = UtilSql.getValue(result, "created");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.updated = UtilSql.getValue(result, "updated");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.iErrormsg = UtilSql.getValue(result, "i_errormsg");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.establecimiento = UtilSql.getValue(result, "establecimiento");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.emision = UtilSql.getValue(result, "emision");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.numero = UtilSql.getValue(result, "numero");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.taxid = UtilSql.getValue(result, "taxid");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.value = UtilSql.getValue(result, "value");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.valor = UtilSql.getValue(result, "valor");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.impuesto = UtilSql.getValue(result, "impuesto");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.autorizacion = UtilSql.getValue(result, "autorizacion");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.vencimiento = UtilSql.getDateValue(result, "vencimiento", "dd-MM-yyyy");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.mPricelistIdr = UtilSql.getValue(result, "m_pricelist_idr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cPaymenttermIdr = UtilSql.getValue(result, "c_paymentterm_idr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.mProductId = UtilSql.getValue(result, "m_product_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.mProductIdr = UtilSql.getValue(result, "m_product_idr");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.sustento = UtilSql.getValue(result, "sustento");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.isactive = UtilSql.getValue(result, "isactive");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.procesado = UtilSql.getValue(result, "procesado");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.procesar = UtilSql.getValue(result, "procesar");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.irecFacturaId = UtilSql.getValue(result, "irec_factura_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.cGlitemId = UtilSql.getValue(result, "c_glitem_id");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.iIsimported = UtilSql.getValue(result, "i_isimported");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.language = UtilSql.getValue(result, "language");
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.adUserClient = "";
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.adOrgClient = "";
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.createdby = "";
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.trBgcolor = "";
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.totalCount = "";
        objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[] = new ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[vector.size()];
    vector.copyInto(objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data);
    return(objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data);
  }

/**
Create a registry
 */
  public static ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[] set(String procesado, String valor, String cPaymenttermId, String value, String irecFacturaId, String sustento, String taxid, String adOrgId, String emision, String procesar, String adClientId, String iErrormsg, String finPaymentmethodId, String isactive, String mPricelistId, String fecha, String cBpartnerId, String cBpartnerIdr, String vencimiento, String updatedby, String updatedbyr, String mProductId, String mProductIdr, String cGlitemId, String createdby, String createdbyr, String autorizacion, String cDoctypeId, String cInvoiceId, String numero, String impuesto, String establecimiento, String iIsimported)    throws ServletException {
    ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[] = new ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[1];
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0] = new ImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data();
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].created = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].createdbyr = createdbyr;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].updated = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].updatedTimeStamp = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].updatedby = updatedby;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].updatedbyr = updatedbyr;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].iErrormsg = iErrormsg;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].adOrgId = adOrgId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].adOrgIdr = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].establecimiento = establecimiento;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].emision = emision;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].numero = numero;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].fecha = fecha;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cBpartnerId = cBpartnerId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cBpartnerIdr = cBpartnerIdr;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].taxid = taxid;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].value = value;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].valor = valor;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].impuesto = impuesto;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].autorizacion = autorizacion;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].vencimiento = vencimiento;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cDoctypeId = cDoctypeId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cDoctypeIdr = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].mPricelistId = mPricelistId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].mPricelistIdr = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cPaymenttermId = cPaymenttermId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cPaymenttermIdr = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].finPaymentmethodId = finPaymentmethodId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].finPaymentmethodIdr = "";
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].mProductId = mProductId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].mProductIdr = mProductIdr;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].sustento = sustento;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].isactive = isactive;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].procesado = procesado;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].procesar = procesar;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].irecFacturaId = irecFacturaId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cInvoiceId = cInvoiceId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].adClientId = adClientId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].cGlitemId = cGlitemId;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].iIsimported = iIsimported;
    objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data[0].language = "";
    return objectImportacionFacturasComisionC59AFDD5F4F74239A5BE293442171D95Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef788EFFC8DD8C48F1A2B3C84757BD3D23_0(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef932EAA7D4F2D425AA4DFF302EC4F6FEB_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef953177793E8B4C309EA5A5E1E9F29971_2(ConnectionProvider connectionProvider, String paramLanguage, String M_Product_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))), '') ) as M_Product_ID FROM M_Product left join (select M_Product_ID, Name from M_Product) table2 on (M_Product.M_Product_ID = table2.M_Product_ID)left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL2 on (table2.M_Product_ID = tableTRL2.M_Product_ID and tableTRL2.AD_Language = ?)  WHERE M_Product.isActive='Y' AND M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Product_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_product_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefAAEB7E7CA142452EA271F8E4A74767AB_3(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE IREC_Factura" +
      "        SET I_Errormsg = (?) , AD_Org_ID = (?) , Establecimiento = (?) , Emision = (?) , Numero = (?) , Fecha = TO_DATE(?) , C_Bpartner_ID = (?) , Taxid = (?) , Value = (?) , Valor = TO_NUMBER(?) , Impuesto = TO_NUMBER(?) , Autorizacion = (?) , Vencimiento = TO_DATE(?) , C_Doctype_ID = (?) , M_Pricelist_ID = (?) , C_Paymentterm_ID = (?) , FIN_Paymentmethod_ID = (?) , M_Product_ID = (?) , Sustento = (?) , Isactive = (?) , Procesado = (?) , Procesar = (?) , Irec_Factura_ID = (?) , C_Invoice_ID = (?) , AD_Client_ID = (?) , C_Glitem_ID = (?) , I_Isimported = (?) , updated = now(), updatedby = ? " +
      "        WHERE IREC_Factura.Irec_Factura_ID = ? " +
      "        AND IREC_Factura.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IREC_Factura.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iErrormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, establecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, impuesto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, autorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vencimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sustento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, irecFacturaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iIsimported);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, irecFacturaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO IREC_Factura " +
      "        (I_Errormsg, AD_Org_ID, Establecimiento, Emision, Numero, Fecha, C_Bpartner_ID, Taxid, Value, Valor, Impuesto, Autorizacion, Vencimiento, C_Doctype_ID, M_Pricelist_ID, C_Paymentterm_ID, FIN_Paymentmethod_ID, M_Product_ID, Sustento, Isactive, Procesado, Procesar, Irec_Factura_ID, C_Invoice_ID, AD_Client_ID, C_Glitem_ID, I_Isimported, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iErrormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, establecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, impuesto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, autorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vencimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sustento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, irecFacturaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iIsimported);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM IREC_Factura" +
      "        WHERE IREC_Factura.Irec_Factura_ID = ? " +
      "        AND IREC_Factura.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IREC_Factura.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM IREC_Factura" +
      "         WHERE IREC_Factura.Irec_Factura_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM IREC_Factura" +
      "         WHERE IREC_Factura.Irec_Factura_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
