/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.recap.recuperacion.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.PriceList;
/**
 * Entity class for entity Irec_Factura (stored in table IREC_Factura).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class IrecFactura extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "IREC_Factura";
    public static final String ENTITY_NAME = "Irec_Factura";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_ISIMPORTED = "isimported";
    public static final String PROPERTY_ERRORMSG = "errormsg";
    public static final String PROPERTY_PROCESADO = "procesado";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_INVOICE = "invoice";
    public static final String PROPERTY_TAXID = "taxid";
    public static final String PROPERTY_BPARTNER = "bpartner";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_DOCTYPE = "doctype";
    public static final String PROPERTY_ESTABLECIMIENTO = "establecimiento";
    public static final String PROPERTY_EMISION = "emision";
    public static final String PROPERTY_NUMERO = "numero";
    public static final String PROPERTY_SUSTENTO = "sustento";
    public static final String PROPERTY_AUTORIZACION = "autorizacion";
    public static final String PROPERTY_VENCIMIENTO = "vencimiento";
    public static final String PROPERTY_PRICELIST = "pricelist";
    public static final String PROPERTY_PAYMENTTERM = "paymentterm";
    public static final String PROPERTY_FINPAYMENTMETHOD = "fINPaymentmethod";
    public static final String PROPERTY_PRODUCT = "product";
    public static final String PROPERTY_VALUE = "value";
    public static final String PROPERTY_IMPUESTO = "impuesto";
    public static final String PROPERTY_GLITEM = "glitem";
    public static final String PROPERTY_VALOR = "valor";
    public static final String PROPERTY_SUCURSAL = "sucursal";
    public static final String PROPERTY_CONDICIONES = "condiciones";
    public static final String PROPERTY_METODO = "metodo";
    public static final String PROPERTY_TAX = "tax";
    public static final String PROPERTY_RETENCION = "retencion";
    public static final String PROPERTY_TIPORETENCION = "tipoRetencion";

    public IrecFactura() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_ISIMPORTED, false);
        setDefaultValue(PROPERTY_PROCESADO, false);
        setDefaultValue(PROPERTY_PROCESAR, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Boolean isImported() {
        return (Boolean) get(PROPERTY_ISIMPORTED);
    }

    public void setImported(Boolean isimported) {
        set(PROPERTY_ISIMPORTED, isimported);
    }

    public String getErrormsg() {
        return (String) get(PROPERTY_ERRORMSG);
    }

    public void setErrormsg(String errormsg) {
        set(PROPERTY_ERRORMSG, errormsg);
    }

    public Boolean isProcesado() {
        return (Boolean) get(PROPERTY_PROCESADO);
    }

    public void setProcesado(Boolean procesado) {
        set(PROPERTY_PROCESADO, procesado);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public Invoice getInvoice() {
        return (Invoice) get(PROPERTY_INVOICE);
    }

    public void setInvoice(Invoice invoice) {
        set(PROPERTY_INVOICE, invoice);
    }

    public String getTaxid() {
        return (String) get(PROPERTY_TAXID);
    }

    public void setTaxid(String taxid) {
        set(PROPERTY_TAXID, taxid);
    }

    public BusinessPartner getBpartner() {
        return (BusinessPartner) get(PROPERTY_BPARTNER);
    }

    public void setBpartner(BusinessPartner bpartner) {
        set(PROPERTY_BPARTNER, bpartner);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public DocumentType getDoctype() {
        return (DocumentType) get(PROPERTY_DOCTYPE);
    }

    public void setDoctype(DocumentType doctype) {
        set(PROPERTY_DOCTYPE, doctype);
    }

    public String getEstablecimiento() {
        return (String) get(PROPERTY_ESTABLECIMIENTO);
    }

    public void setEstablecimiento(String establecimiento) {
        set(PROPERTY_ESTABLECIMIENTO, establecimiento);
    }

    public String getEmision() {
        return (String) get(PROPERTY_EMISION);
    }

    public void setEmision(String emision) {
        set(PROPERTY_EMISION, emision);
    }

    public String getNumero() {
        return (String) get(PROPERTY_NUMERO);
    }

    public void setNumero(String numero) {
        set(PROPERTY_NUMERO, numero);
    }

    public String getSustento() {
        return (String) get(PROPERTY_SUSTENTO);
    }

    public void setSustento(String sustento) {
        set(PROPERTY_SUSTENTO, sustento);
    }

    public String getAutorizacion() {
        return (String) get(PROPERTY_AUTORIZACION);
    }

    public void setAutorizacion(String autorizacion) {
        set(PROPERTY_AUTORIZACION, autorizacion);
    }

    public Date getVencimiento() {
        return (Date) get(PROPERTY_VENCIMIENTO);
    }

    public void setVencimiento(Date vencimiento) {
        set(PROPERTY_VENCIMIENTO, vencimiento);
    }

    public PriceList getPricelist() {
        return (PriceList) get(PROPERTY_PRICELIST);
    }

    public void setPricelist(PriceList pricelist) {
        set(PROPERTY_PRICELIST, pricelist);
    }

    public PaymentTerm getPaymentterm() {
        return (PaymentTerm) get(PROPERTY_PAYMENTTERM);
    }

    public void setPaymentterm(PaymentTerm paymentterm) {
        set(PROPERTY_PAYMENTTERM, paymentterm);
    }

    public FIN_PaymentMethod getFINPaymentmethod() {
        return (FIN_PaymentMethod) get(PROPERTY_FINPAYMENTMETHOD);
    }

    public void setFINPaymentmethod(FIN_PaymentMethod fINPaymentmethod) {
        set(PROPERTY_FINPAYMENTMETHOD, fINPaymentmethod);
    }

    public Product getProduct() {
        return (Product) get(PROPERTY_PRODUCT);
    }

    public void setProduct(Product product) {
        set(PROPERTY_PRODUCT, product);
    }

    public String getValue() {
        return (String) get(PROPERTY_VALUE);
    }

    public void setValue(String value) {
        set(PROPERTY_VALUE, value);
    }

    public BigDecimal getImpuesto() {
        return (BigDecimal) get(PROPERTY_IMPUESTO);
    }

    public void setImpuesto(BigDecimal impuesto) {
        set(PROPERTY_IMPUESTO, impuesto);
    }

    public GLItem getGlitem() {
        return (GLItem) get(PROPERTY_GLITEM);
    }

    public void setGlitem(GLItem glitem) {
        set(PROPERTY_GLITEM, glitem);
    }

    public BigDecimal getValor() {
        return (BigDecimal) get(PROPERTY_VALOR);
    }

    public void setValor(BigDecimal valor) {
        set(PROPERTY_VALOR, valor);
    }

    public String getSucursal() {
        return (String) get(PROPERTY_SUCURSAL);
    }

    public void setSucursal(String sucursal) {
        set(PROPERTY_SUCURSAL, sucursal);
    }

    public String getCondiciones() {
        return (String) get(PROPERTY_CONDICIONES);
    }

    public void setCondiciones(String condiciones) {
        set(PROPERTY_CONDICIONES, condiciones);
    }

    public String getMetodo() {
        return (String) get(PROPERTY_METODO);
    }

    public void setMetodo(String metodo) {
        set(PROPERTY_METODO, metodo);
    }

    public TaxRate getTax() {
        return (TaxRate) get(PROPERTY_TAX);
    }

    public void setTax(TaxRate tax) {
        set(PROPERTY_TAX, tax);
    }

    public String getRetencion() {
        return (String) get(PROPERTY_RETENCION);
    }

    public void setRetencion(String retencion) {
        set(PROPERTY_RETENCION, retencion);
    }

    public String getTipoRetencion() {
        return (String) get(PROPERTY_TIPORETENCION);
    }

    public void setTipoRetencion(String tipoRetencion) {
        set(PROPERTY_TIPORETENCION, tipoRetencion);
    }

}
