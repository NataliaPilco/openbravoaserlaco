/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.recap.data;

import com.atrums.recap.recuperacion.data.irecArchivo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
/**
 * Entity class for entity ATREC_RecapLinea (stored in table atrec_recap_linea).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class atrecRecapLinea extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "atrec_recap_linea";
    public static final String ENTITY_NAME = "ATREC_RecapLinea";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_ATRECRECAP = "atrecRecap";
    public static final String PROPERTY_LINE = "line";
    public static final String PROPERTY_FINPAYMENTMETHOD = "fINPaymentmethod";
    public static final String PROPERTY_CONCEPTO = "concepto";
    public static final String PROPERTY_LOTE = "lote";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_VALORREPORTE = "valorReporte";
    public static final String PROPERTY_VALOR = "valor";
    public static final String PROPERTY_FINFINANCIALACCOUNT = "fINFinancialAccount";
    public static final String PROPERTY_GLITEM = "glitem";
    public static final String PROPERTY_DESCRIPCION = "descripcion";
    public static final String PROPERTY_FINFINTRACCOBRO = "fINFintracCobro";
    public static final String PROPERTY_FINFINTRACPAGO = "fINFintracPago";
    public static final String PROPERTY_TIPOPAGO = "tipoPago";
    public static final String PROPERTY__COMPUTEDCOLUMNS = "_computedColumns";
    public static final String PROPERTY_ATRECRECAPDETALLELIST = "aTRECRecapDetalleList";
    public static final String PROPERTY_ATRECRECUPERALINEALIST = "aTRECRecuperaLineaList";
    public static final String PROPERTY_IRECARCHIVOLIST = "iRECArchivoList";


    // Computed columns properties, these properties cannot be directly accessed, they need
    // to be read through _commputedColumns proxy. They cannot be directly used in HQL, OBQuery
    // nor OBCriteria. 
    public static final String COMPUTED_COLUMN_VALORRECAP = "valorRecap";

    public atrecRecapLinea() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_VALORREPORTE, new BigDecimal(0));
        setDefaultValue(PROPERTY_ATRECRECAPDETALLELIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_ATRECRECUPERALINEALIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_IRECARCHIVOLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public atrecRecap getAtrecRecap() {
        return (atrecRecap) get(PROPERTY_ATRECRECAP);
    }

    public void setAtrecRecap(atrecRecap atrecRecap) {
        set(PROPERTY_ATRECRECAP, atrecRecap);
    }

    public Long getLine() {
        return (Long) get(PROPERTY_LINE);
    }

    public void setLine(Long line) {
        set(PROPERTY_LINE, line);
    }

    public FIN_PaymentMethod getFINPaymentmethod() {
        return (FIN_PaymentMethod) get(PROPERTY_FINPAYMENTMETHOD);
    }

    public void setFINPaymentmethod(FIN_PaymentMethod fINPaymentmethod) {
        set(PROPERTY_FINPAYMENTMETHOD, fINPaymentmethod);
    }

    public String getConcepto() {
        return (String) get(PROPERTY_CONCEPTO);
    }

    public void setConcepto(String concepto) {
        set(PROPERTY_CONCEPTO, concepto);
    }

    public String getLote() {
        return (String) get(PROPERTY_LOTE);
    }

    public void setLote(String lote) {
        set(PROPERTY_LOTE, lote);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public BigDecimal getValorReporte() {
        return (BigDecimal) get(PROPERTY_VALORREPORTE);
    }

    public void setValorReporte(BigDecimal valorReporte) {
        set(PROPERTY_VALORREPORTE, valorReporte);
    }

    public BigDecimal getValor() {
        return (BigDecimal) get(PROPERTY_VALOR);
    }

    public void setValor(BigDecimal valor) {
        set(PROPERTY_VALOR, valor);
    }

    public FIN_FinancialAccount getFINFinancialAccount() {
        return (FIN_FinancialAccount) get(PROPERTY_FINFINANCIALACCOUNT);
    }

    public void setFINFinancialAccount(FIN_FinancialAccount fINFinancialAccount) {
        set(PROPERTY_FINFINANCIALACCOUNT, fINFinancialAccount);
    }

    public GLItem getGlitem() {
        return (GLItem) get(PROPERTY_GLITEM);
    }

    public void setGlitem(GLItem glitem) {
        set(PROPERTY_GLITEM, glitem);
    }

    public String getDescripcion() {
        return (String) get(PROPERTY_DESCRIPCION);
    }

    public void setDescripcion(String descripcion) {
        set(PROPERTY_DESCRIPCION, descripcion);
    }

    public FIN_FinaccTransaction getFINFintracCobro() {
        return (FIN_FinaccTransaction) get(PROPERTY_FINFINTRACCOBRO);
    }

    public void setFINFintracCobro(FIN_FinaccTransaction fINFintracCobro) {
        set(PROPERTY_FINFINTRACCOBRO, fINFintracCobro);
    }

    public FIN_FinaccTransaction getFINFintracPago() {
        return (FIN_FinaccTransaction) get(PROPERTY_FINFINTRACPAGO);
    }

    public void setFINFintracPago(FIN_FinaccTransaction fINFintracPago) {
        set(PROPERTY_FINFINTRACPAGO, fINFintracPago);
    }

    public BigDecimal getValorRecap() {
        return (BigDecimal) get(COMPUTED_COLUMN_VALORRECAP);
    }

    public void setValorRecap(BigDecimal valorRecap) {
        set(COMPUTED_COLUMN_VALORRECAP, valorRecap);
    }

    public String getTipoPago() {
        return (String) get(PROPERTY_TIPOPAGO);
    }

    public void setTipoPago(String tipoPago) {
        set(PROPERTY_TIPOPAGO, tipoPago);
    }

    public atrecRecapLinea_ComputedColumns get_computedColumns() {
        return (atrecRecapLinea_ComputedColumns) get(PROPERTY__COMPUTEDCOLUMNS);
    }

    public void set_computedColumns(atrecRecapLinea_ComputedColumns _computedColumns) {
        set(PROPERTY__COMPUTEDCOLUMNS, _computedColumns);
    }

    @SuppressWarnings("unchecked")
    public List<atrecRecapDetalle> getATRECRecapDetalleList() {
      return (List<atrecRecapDetalle>) get(PROPERTY_ATRECRECAPDETALLELIST);
    }

    public void setATRECRecapDetalleList(List<atrecRecapDetalle> aTRECRecapDetalleList) {
        set(PROPERTY_ATRECRECAPDETALLELIST, aTRECRecapDetalleList);
    }

    @SuppressWarnings("unchecked")
    public List<atrecRecuperaLinea> getATRECRecuperaLineaList() {
      return (List<atrecRecuperaLinea>) get(PROPERTY_ATRECRECUPERALINEALIST);
    }

    public void setATRECRecuperaLineaList(List<atrecRecuperaLinea> aTRECRecuperaLineaList) {
        set(PROPERTY_ATRECRECUPERALINEALIST, aTRECRecuperaLineaList);
    }

    @SuppressWarnings("unchecked")
    public List<irecArchivo> getIRECArchivoList() {
      return (List<irecArchivo>) get(PROPERTY_IRECARCHIVOLIST);
    }

    public void setIRECArchivoList(List<irecArchivo> iRECArchivoList) {
        set(PROPERTY_IRECARCHIVOLIST, iRECArchivoList);
    }


    @Override
    public Object get(String propName) {
      if (COMPUTED_COLUMN_VALORRECAP.equals(propName)) {
        if (get_computedColumns() == null) {
          return null;
        }
        return get_computedColumns().getValorRecap();
      }
    
      return super.get(propName);
    }
}
